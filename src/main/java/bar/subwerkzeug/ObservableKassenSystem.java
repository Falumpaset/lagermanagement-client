package bar.subwerkzeug;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import bar.Fachwerte.Produkt;






public abstract class ObservableKassenSystem
{
    private Set<KassenObserver> _alleBeobachter;

    public ObservableKassenSystem()

    /**
     * Initialisiert ein beobachtbares Subwerkzeug.
     */
    {
        _alleBeobachter = new HashSet<KassenObserver>();
    }

    /**
     * Registriert einen Beobachter an diesem Subwerkzeug. Der Beobachter wird
     * informiert, wenn sich bei diesem Werkzeug etwas ändert.
     * 
     * @require beobachter != null
     */
    public void registriereBeobachter(KassenObserver beobachter)
    {
        assert beobachter != null : "Vorbedingung verletzt: beobachter != null";
        _alleBeobachter.add(beobachter);
    }

    /**
     * Entfernt einen Beobachter dieses Subwerkzeugs. Wenn der Beobachter gar
     * nicht registriert war, passiert nichts.
     */
    public void entferneBeobachter(KassenObserver beobachter)
    {
        _alleBeobachter.remove(beobachter);
    }

    /**
     * Informiert alle an diesem Subwerkzeug registrierten Beobachter über eine
     * Änderung.
     * 
     * Diese Methode muss von der erbenden Klasse immer dann aufgerufen werden,
     * wenn eine Änderung geschehen ist, die für Beobachter interessant ist.
     */
    protected void bestandsAenderung(Map<Produkt, Integer> verkaufteProdukte)
    {
        for (KassenObserver beobachter : _alleBeobachter)
        {
            beobachter.reagiereAufAenderung(verkaufteProdukte);
        }
    }

}
