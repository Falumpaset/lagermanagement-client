package bar.Verkauf.Ui.SubwerkzeugObserver;

import java.util.Map;

import bar.Fachwerte.Produkt;



public interface KassenObserver
{

    void reagiereAufAenderung(Map<Produkt, Integer> verkaufteProdukte);

}
