package bar.Verkauf.Ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

public class NummernWerkzeugUI
{
    private JPanel _panel;

    private JPanel _panelNormal;
    private JPanel _panelBezahlen;
    private JPanel _hold;

    private JPanel _numberPadBezahlen;
    private JPanel _euroPanel;
    private JPanel _mengenPad;

    private Dimension _fensterGr;

    private JTextField _preisField;
    private JTextField _gezahltField;
    private JTextField _restField;
    private JTextField _preisFieldBezahlen;

    private JLabel _restBetragLabel;

    private static final Color HINTERGRUNDFARBE = new Color(30, 30, 30);
    private static final Color SCHRIFTFARBE_NORMAL = new Color(95, 247, 0);
    private static final Color SCHRIFTFARBE_FEHLER = new Color(255, 148, 148);

    private static final Font SCHRIFTART_GROSS = new Font("Monospaced",
            Font.BOLD, 28);

    private JButton _bezahlBut;
    private JButton _abbBut;
    private JButton _barBut;
    private JButton _karteBut;
    private JButton _enterBut;
    private JButton _bezAbbBut;

    public NummernWerkzeugUI()
    {

        _panelNormal = erstellePanelNormal();
        _panelBezahlen = erstellePanelBezahlen();
        _panel = erstelleMainPanel();

    }

    //erstellt das Mainpanel im Cardlayout mit zwei unteschiedlichen Panels, die über ihren string identifiziert werden
    private JPanel erstelleMainPanel()
    {
        JPanel panel = new JPanel();

        panel.setLayout(new CardLayout());
        panel.add(_panelNormal, "1");
        panel.add(_panelBezahlen, "2");
        return panel;
    }

    private JPanel erstellePanelNormal()
    {
        setDimension();

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        erstelleNumberPad();
        panel.add(_mengenPad);

        erstellePreisLabel();
        panel.add(erstelleLayoutPanel("Preis", _preisField));
        panel.add(erstelleButtonSegment());

        return panel;

    }

    private void erstelleNumberPad()
    {
        _mengenPad = new JPanel(new GridLayout(5, 2));
        for (int i = 1; i < 10; i++)
        {
            String titel = "" + i;
            JButton but = new MengenButton(titel, i);
            but.setPreferredSize(
                    new Dimension(0, (int) _fensterGr.getHeight() / 10));
            _mengenPad.add(but);

        }
        _mengenPad.add(new MengenButton("0", 0));
        Border border = BorderFactory.createTitledBorder("Menge");

        _mengenPad.setBorder(border);

    }

    // erstellt das JTF Preis für die normals anischt
    private void erstellePreisLabel()
    {

        _preisField = new JTextField();
        _preisField.setBackground(HINTERGRUNDFARBE);
        _preisField.setEditable(false);

        //kosmetische einstellungen
        _preisField.setHorizontalAlignment(SwingConstants.CENTER);
        _preisField.setCaretColor(SCHRIFTFARBE_NORMAL);
        _preisField.setForeground(SCHRIFTFARBE_NORMAL);
        _preisField.setFont(SCHRIFTART_GROSS);

        //setsize
        _preisField.setPreferredSize(new Dimension(_fensterGr.width / 3,
                (int) _fensterGr.getHeight() / 10));

    }

    private JPanel erstelleButtonSegment()
    {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

        JPanel abbButtonPanel = new JPanel();

        abbButtonPanel.setLayout(new GridLayout(2, 1));

        _bezahlBut = new JButton("Bezahlen");
        _abbBut = new JButton("Abbruch");

        //TODO color

        abbButtonPanel.add(_bezahlBut);
        abbButtonPanel.add(_abbBut);

        buttonPanel.add(abbButtonPanel);

        buttonPanel.setPreferredSize(
                new Dimension(0, (int) _fensterGr.getHeight() / 3));

        return buttonPanel;

    }

    //deklariert das TF für den preis für das Bezahlwerkzeug
    private void erstellePreisLabelBezahlen()
    {

        _preisFieldBezahlen = new JTextField();
        _preisFieldBezahlen.setBackground(HINTERGRUNDFARBE);
        _preisFieldBezahlen.setEditable(false);

        //kosmetische einstellungen
        _preisFieldBezahlen.setHorizontalAlignment(SwingConstants.CENTER);
        _preisFieldBezahlen.setCaretColor(SCHRIFTFARBE_NORMAL);
        _preisFieldBezahlen.setForeground(SCHRIFTFARBE_NORMAL);
        _preisFieldBezahlen.setFont(SCHRIFTART_GROSS);

    }

    // erstellt die bezahlenansicht mit dem numberpad drei textfields und den buttons
    public JPanel erstellePanelBezahlen()
    {
        JPanel panel = new JPanel();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        erstellePreisLabelBezahlen();
        panel.add(erstelleLayoutPanel("Preis", _preisFieldBezahlen));

        erstelleGezahltTextField();
        panel.add(erstelleLayoutPanel("Gezahlt", _gezahltField));

        erstelleRestbetragTextField();
        panel.add(erstelleRestBetragPanel());
        panel.add(erstelleBarEinzahlungsPanel());
        panel.add(erstelleButtonPanelBezahlen());
        panel.setPreferredSize(
                new Dimension(0, (int) _fensterGr.getHeight() / 2));
        return panel;
    }

    private JPanel erstelleRestBetragPanel()
    {
        JPanel restbetragPanel = new JPanel(new BorderLayout());
        _restBetragLabel = new JLabel("Noch zu zahlen");
        _restBetragLabel.setHorizontalAlignment(SwingConstants.CENTER);
        restbetragPanel.add(_restBetragLabel, BorderLayout.NORTH);
        restbetragPanel.add(_restField, BorderLayout.CENTER);
        return restbetragPanel;

    }

    private void erstelleGezahltTextField()
    {

        _gezahltField = new JTextField();
        _gezahltField.setCaretColor(SCHRIFTFARBE_NORMAL);
        _gezahltField.setHorizontalAlignment(SwingConstants.CENTER);
        _gezahltField.setBackground(HINTERGRUNDFARBE);
        _gezahltField.setForeground(SCHRIFTFARBE_NORMAL);
        _gezahltField.setFont(SCHRIFTART_GROSS);
        _gezahltField.requestFocus(true);
    }

    private JPanel erstelleBarEinzahlungsPanel()
    {
        _hold = new JPanel();
        _hold.setLayout(new GridLayout(1, 2));
        _numberPadBezahlen = new JPanel();
        _numberPadBezahlen.setLayout(new GridLayout(4, 3));

        for (int i = 1; i < 10; i++)
        {
            String titel = "" + i;
            JButton but = new JButton(titel);
            but.setPreferredSize(
                    new Dimension(0, (int) _fensterGr.getHeight() / 20));
            _numberPadBezahlen.add(but);

        }

        _numberPadBezahlen.add(new JButton("0"));
        _numberPadBezahlen.add(new JButton("."));
        _numberPadBezahlen.add(new JButton("<-"));

        _euroPanel = new JPanel();
        _euroPanel.setLayout(new GridLayout(4, 1));
        _euroPanel.add(new JButton("100"));
        _euroPanel.add(new JButton("50"));

        _euroPanel.add(new JButton("20"));
        _euroPanel.add(new JButton("10"));

        _hold.add(_numberPadBezahlen);
        _hold.add(_euroPanel);

        return _hold;
    }

    public void markiereRestBetrag(boolean genuegend)
    {
        if (genuegend)
        {
            _restBetragLabel.setText("Rueckgeld");
            _restField.setForeground(SCHRIFTFARBE_NORMAL);
        }
        else
        {
            _restBetragLabel.setText("Noch zu Zahlen");
            _restField.setForeground(SCHRIFTFARBE_FEHLER);
        }
    }

    private void erstelleRestbetragTextField()
    {
        _restField = new JTextField();
        _restField.setCaretColor(SCHRIFTFARBE_NORMAL);
        _restField.setHorizontalAlignment(SwingConstants.CENTER);
        _restField.setBackground(HINTERGRUNDFARBE);
        _restField.setForeground(SCHRIFTFARBE_NORMAL);
        _restField.setFont(SCHRIFTART_GROSS);
        _restField.setEditable(false);
        _restField.setFocusable(false);

    }

    //erzeugt ein JPanel mit Titel und ausrichtung
    private Component erstelleLayoutPanel(String titel, JComponent component)
    {
        JPanel rueckgabePanel = new JPanel(new BorderLayout());

        JLabel label = new JLabel(titel);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        rueckgabePanel.add(label, BorderLayout.NORTH);
        rueckgabePanel.add(component, BorderLayout.CENTER);

        return rueckgabePanel;
    }

    // buttons für die bezahlansicht
    private JPanel erstelleButtonPanelBezahlen()
    {
        JPanel panel = new JPanel();

        panel.setLayout(new GridLayout(2, 2));

        _barBut = new JButton("Bar");
        _enterBut = new JButton("Fertig");
        _karteBut = new JButton("Karte");
        _bezAbbBut = new JButton("Abbruch");

        panel.add(erstelleButtonLayout(_barBut));
        panel.add(erstelleButtonLayout(_karteBut));
        panel.add(erstelleButtonLayout(_bezAbbBut));
        panel.add(erstelleButtonLayout(_enterBut));
        return panel;
    }

    private JButton erstelleButtonLayout(JButton but)
    {
        but.setPreferredSize(
                new Dimension(0, (int) _fensterGr.getHeight() / 10));
        return but;
    }

    //erstellt eine neue Dimension anhand der akutellen screensize
    private Dimension setDimension()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        _fensterGr = toolkit.getScreenSize();

        return _fensterGr;
    }

    //liefert die panels, in die die Buttons integriert sind
    public JPanel getPanel()
    {
        return _panel;
    }

    public JPanel getZiffernPad()
    {
        return _hold;
    }

    public JPanel getZahlen()
    {
        return _numberPadBezahlen;
    }

    public JPanel getScheine()
    {
        return _euroPanel;
    }

    public JPanel getMengenPad()
    {
        return _mengenPad;
    }

    //liefert die funktionalen Buttons der UI
    public JButton getBezahlBut()
    {
        return _bezahlBut;
    }

    public JButton getAbbBut()
    {
        return _abbBut;
    }

    public JButton getBarButton()
    {
        return _barBut;
    }

    public JButton getEnterBut()
    {
        return _enterBut;
    }

    public JButton getGezAbbBut()
    {
        return _bezAbbBut;
    }

    public JButton getKarteBut()
    {
        return _karteBut;
    }

    // liefert die Textfields der UI
    public JTextField getPreisField()
    {
        return _preisField;
    }

    public JTextField getPreisFieldBezahlen()
    {
        return _preisFieldBezahlen;
    }

    public JTextField getRestbetragField()
    {
        return _restField;
    }

    public JTextField getGezahltField()
    {
        return _gezahltField;
    }

}
