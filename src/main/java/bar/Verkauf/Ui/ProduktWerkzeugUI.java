package bar.Verkauf.Ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.Border;

import bar.Fachwerte.Produkt;
import bar.Fachwerte.Produkt.Kategorie;
import bar.Verkauf.Ui.SubwerkzeugObserver.ObservableSubwerkzeug;


public class ProduktWerkzeugUI extends ObservableSubwerkzeug
{
    private JPanel _panel;
    private JPanel _katPanel;
    private JPanel _produktPanel;
    private JPanel _buttonPanel;
    private JList<String> _infList;
    private JButton _informationBut;

    private TreeSet<Produkt> _produkte;
    private List<ProduktButton> _produktButtons;
    private List<KategorieButton> _kategorieButtons;
    private TreeSet<Kategorie> _kategorien;

    private Dimension _buttonGr;

    public ProduktWerkzeugUI(TreeSet<Produkt> produkte,
            TreeSet<Kategorie> kategorien, DefaultListModel<String> information)
    {

        setButtonDimension();
        _produkte = produkte;
        _kategorien = kategorien;
        _panel = erstellePanel();
        erstelleInformationsList(information);

    }

    private JPanel erstellePanel()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(erstelleKategoriePanel(), BorderLayout.NORTH);
        panel.add(erstelleProduktPanel(), BorderLayout.CENTER);
        panel.add(erstelleButtonPanel(), BorderLayout.SOUTH);
        return panel;
    }

    private List<ProduktButton> erstelleProduktButtons()
    {
        _produktButtons = new ArrayList<ProduktButton>();
        for (Produkt get : _produkte)
        {

            _produktButtons.add(new ProduktButton(get));

        }

        return _produktButtons;
    }

    /**
     * Diese Methode erstellt abhängig von den Kategorien mehrere Panels und ordnet die Buttons dementsprechend zu
     * Sie kopiert alle erstellen Buttons, um sie dann dem AllgemeinPanel hinzuzufügen
     * @return Das Panel im Cardlayout, das die einzelnen Kategoriepanels hält
     */
    private JPanel erstelleProduktPanel()
    {
        _produktPanel = new JPanel();
        _produktPanel.setLayout(new CardLayout());
        erstelleProduktButtons();
        List<ProduktButton> alleButtons = new ArrayList<ProduktButton>();

        for (ProduktButton button : _produktButtons)
        {
            ProduktButton newButton = new ProduktButton(button.getProdukt());
            alleButtons.add(newButton);
        }

        for (Kategorie kat : _kategorien)
        {

            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(4, 0, 20, 20));

            Border border = BorderFactory.createTitledBorder("Produkte");
            panel.setBorder(border);
            _produktPanel.add(panel, kat.toString());

            switch (kat)
            {
            case Allgemein:
                for (ProduktButton b : alleButtons)
                {
                    panel.add(b);
                    b.setPreferredSize(_buttonGr);

                }
                break;

            default:
                for (ProduktButton but : _produktButtons)
                {

                    if (but.getProdukt()
                        .getKategorie()
                        .equals(kat))
                    {
                        but.setPreferredSize(_buttonGr);
                        panel.add(but);

                    }
                }
                break;
            }

        }
        for (ProduktButton a : alleButtons)
        {

            _produktButtons.add(a);
        }

        return _produktPanel;

    }

    private JPanel erstelleKategoriePanel()
    {
        _katPanel = new JPanel();
        _katPanel.setLayout(new GridLayout(1, 0));
        for (KategorieButton but : erstelleKategorieButtons())
        {

            _katPanel.add(but);
        }

        Border border = BorderFactory.createTitledBorder("Kategorien");
        _katPanel.setBorder(border);

        return _katPanel;
    }

    private JPanel erstelleButtonPanel()
    {
        _buttonPanel = new JPanel();
        _buttonPanel.setLayout(new GridLayout(1, 2));
        _informationBut = new JButton("Information");
        _informationBut.setPreferredSize(new Dimension(0, 114));
        _buttonPanel.add(_informationBut);

        return _buttonPanel;
    }

    private List<KategorieButton> erstelleKategorieButtons()
    {
        _kategorieButtons = new ArrayList<KategorieButton>();

        for (Kategorie kat : _kategorien)
        {
            KategorieButton but = new KategorieButton(kat);
            _kategorieButtons.add(but);
        }

        return _kategorieButtons;
    }

    private void erstelleInformationsList(DefaultListModel<String> list)
    {
        _infList = new JList<String>(list);
        _produktPanel.add(_infList, "information");
    }

    //erstellt eine neue Dimension anhand der akutellen screensize
    private void setButtonDimension()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dim = toolkit.getScreenSize();
        _buttonGr = new Dimension((int) dim.getWidth() / 10,
                (int) dim.getWidth() / 10);

    }

    public List<ProduktButton> getProduktButtons()
    {
        return _produktButtons;
    }

    public JList<String> getInfoList()
    {
        return _infList;
    }

    public List<KategorieButton> getKategorieButtons()
    {
        return _kategorieButtons;
    }

    public JPanel getKategoriePanel()
    {
        return _katPanel;
    }

    public JButton getInfoBut()
    {
        return _informationBut;
    }

    public JPanel getProduktPanel()
    {
        return _produktPanel;
    }

    public JPanel getPanel()

    {
        return _panel;
    }

}
