package bar.Verkauf.Ui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;

public class MengenButton extends JButton
{

    /**
     * 
     */
    private static final long serialVersionUID = 4822567807041973206L;
    private int _menge;

    public MengenButton(String name, int menge)
    {
        super(name);
        _menge = menge;
        setMaximumSize(setButtonGroeße());

    }

    public int getMenge()
    {
        return _menge;
    }

    private Dimension setButtonGroeße()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension _fensterGr = toolkit.getScreenSize();

        return new Dimension(0, (int) _fensterGr.getHeight() / 10);
    }
}
