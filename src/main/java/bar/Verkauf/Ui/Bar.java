package bar.Verkauf.Ui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import bar.Fachwerte.Geldbetrag;
import bar.Fachwerte.Produkt;
import bar.Fachwerte.Produkt.Kategorie;

public class Bar
{
    private HashSet<Kategorie> _kategorien;
    private List<Produkt> _getraenke;

    public Bar(List<Produkt> getraenke)
    {
        _getraenke = getraenke;

    }

    public TreeSet<Produkt> getGetraenke()
    {
        TreeSet<Produkt> getraenkeSortiert = new TreeSet<Produkt>(
                new Comparator<Produkt>()
                {

                    @Override
                    public int compare(Produkt o1, Produkt o2)
                    {
                        return o1.getName()
                            .compareTo(o2.getName());
                    }
                });

        getraenkeSortiert.addAll(_getraenke);

        return getraenkeSortiert;
    }

    public TreeSet<Kategorie> getKategorien()
    {
        _kategorien = new HashSet<Kategorie>();
        _kategorien.add(Kategorie.Allgemein);
        for (Produkt get : _getraenke)
        {
            _kategorien.add(get.getKategorie());
        }

        TreeSet<Kategorie> _kategorienSortiert = new TreeSet<Produkt.Kategorie>(
                new Comparator<Kategorie>()
                {

                 
					@Override
                    public int compare(Kategorie o1, Kategorie o2)
                    {
                        return o1.toString()
                            .compareTo(o2.toString());

                    }

                });
        _kategorienSortiert.addAll(_kategorien);

        return _kategorienSortiert;
    }

    public List<String> getNamen()
    {
        List<String> namenListe = new ArrayList<String>();
        for (Produkt get : _getraenke)
        {
            namenListe.add(get.getFormatiertenString());
        }
        return namenListe;
    }

    public int anzahlGetraenke()
    {
        return _getraenke.size();
    }



    public Geldbetrag getPreisFuer(Produkt produkt)
    {
        return produkt.getPreis();
    }

}
