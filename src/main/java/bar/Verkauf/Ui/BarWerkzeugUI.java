package bar.Verkauf.Ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class BarWerkzeugUI
{

    private static final String TITEL = "Nur der HSV";

    private JFrame _frame;

    private Dimension _fensterGr;

    private JComponent rechtsPanel;
    private JComponent linksPanel;
    private JComponent mittelPanel;

    public BarWerkzeugUI(JPanel produktPanel, JPanel auflister, JPanel nummer)

    {

        _frame = new JFrame(TITEL);
        _frame.setLayout(new BorderLayout());
        _frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        rechtsPanel = produktPanel;
        mittelPanel = nummer;
        linksPanel = auflister;

        fuegeComponentenHinzu();

    }

    public void zeigeFenster()
    {

        _frame.setVisible(true);

        _frame.setSize(setDimension());
        setSizes();
        _frame.pack();

    }

    private JMenuBar addMenuBar()
    {
        JMenuBar menubar = new JMenuBar();
        JMenu menu = new JMenu("Fenster");
        menu.add(new JMenuItem("Fenstergroeße"));
        menubar.add(menu);

        return menubar;
    }

    private Dimension setDimension()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        _fensterGr = toolkit.getScreenSize();

        return _fensterGr;
    }

    private void setSizes()
    {
        Dimension compDim = new Dimension(_fensterGr.width / 3,
                _fensterGr.height);

        rechtsPanel.setPreferredSize(compDim);
        linksPanel.setPreferredSize(compDim);
        mittelPanel.setPreferredSize(compDim);

    }

    private void fuegeComponentenHinzu()
    {
        _frame.getContentPane()
            .add(rechtsPanel, BorderLayout.EAST);
        _frame.getContentPane()
            .add(linksPanel, BorderLayout.WEST);
        _frame.setJMenuBar(addMenuBar());
        _frame.getContentPane()
            .add(mittelPanel, BorderLayout.CENTER);

    }
}
