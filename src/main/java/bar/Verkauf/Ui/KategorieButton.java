package bar.Verkauf.Ui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;

import bar.Fachwerte.Produkt.Kategorie;

public class KategorieButton extends JButton

{

    /**
     * Erweiterung des JButtons um eine Kategorie, um Produkte zu sortieren
     */
    private static final long serialVersionUID = -733765555889211694L;
    private Kategorie _kategorie;

    public KategorieButton(Kategorie kategorie)
    {
        super(kategorie.toString());
        _kategorie = kategorie;
        setSize();
    }

    //setzt die größe der Buttons auf 1/10 der horizontalen Screensize
    private void setSize()
    {
        setPreferredSize(
                new Dimension(0, (int) getFrameDimension().getHeight() / 10));
    }

    private Dimension getFrameDimension()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension _fensterGr = toolkit.getScreenSize();
        return _fensterGr;
    }

    public Kategorie getKategorie()
    {
        return _kategorie;
    }

}
