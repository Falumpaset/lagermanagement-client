package bar.Verkauf.Ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import bar.Services.BarLink;



public class StartPanelWerkzeug
{

    private StartPanelUI _ui;

    public StartPanelWerkzeug()
    {
        _ui = new StartPanelUI();
        
        erstelleButtonListender();
       

    }

    private void erstelleButtonListender()
    {
        _ui.getBarBut()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                   
                }
            });

        _ui.getBarWerkzeugButton()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
       
                var get = BarLink.getProdukte();
                new BarWerkzeug(new Bar (get));
                _ui.verbergeUI();

                }
            });
        
        

        _ui.getLagerButton()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    // TODO Auto-generated method stub

                }
            });

        _ui.getManagementButton()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    // TODO Auto-generated method stub

                }
            });
    }

}
