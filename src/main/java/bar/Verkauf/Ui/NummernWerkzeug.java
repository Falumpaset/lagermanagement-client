package bar.Verkauf.Ui;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bar.Fachwerte.Geldbetrag;
import bar.Verkauf.Ui.SubwerkzeugObserver.ObservableSubwerkzeug;


public class NummernWerkzeug extends ObservableSubwerkzeug
{
    private NummernWerkzeugUI _ui;

    private JPanel _uiPanel;

    private JTextField _gezahltField;

    private JTextField _restBetragField;

    private JTextField _preisField;

    private JTextField _preisFieldBezahlen;

    private ArrayList<MengenButton> _gedrueckteButtons;

    private boolean _mehrereButtonsGedrueckt = false;

    private ZahlungsArt _zahlungsArt;

    public enum ZahlungsArt
    {
        ECZahlung, Bar;
    }

    public NummernWerkzeug()
    {
        _ui = new NummernWerkzeugUI();

        //feldzuweisung
        _uiPanel = _ui.getPanel();
        _gezahltField = _ui.getGezahltField();
        _restBetragField = _ui.getRestbetragField();
        _preisField = _ui.getPreisField();
        _preisField.setText("0.00");

        _gedrueckteButtons = new ArrayList<MengenButton>();

        _preisFieldBezahlen = _ui.getPreisFieldBezahlen();

        //listener
        registiereUIAktionen();
        registriereNumberPadAktionen();
        registiereAktionenFuerMengenWerkzeug();

        resetUI();
    }

    private void registiereUIAktionen()
    {
        _ui.getBezahlBut()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    _preisFieldBezahlen.setText(_preisField.getText());

                    _restBetragField.setText(_preisFieldBezahlen.getText());
                    switchLayout(2);

                }
            });
        _ui.getGezAbbBut()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {

                    switchLayout(1);
                    resetUI();

                }
            });

        _ui.getBarButton()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    if (_gezahltField.getText()
                        .length() == 0)
                    {

                        _gezahltField.setText(_preisFieldBezahlen.getText());

                        berechneRestbetrag(_gezahltField.getText());

                    }
                    else
                    {
                        _gezahltField.setText(
                                ergaenzeKommaStellen(_gezahltField.getText()));
                        berechneRestbetrag(
                                ergaenzeKommaStellen(_gezahltField.getText()));
                    }
                    _zahlungsArt = ZahlungsArt.Bar;
                    _ui.getEnterBut()
                        .setEnabled(true);
                }

            });

        _ui.getKarteBut()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    _gezahltField
                        .setText(new Geldbetrag(_preisFieldBezahlen.getText())
                            .toString());
                    berechneRestbetrag(_gezahltField.getText());
                    _zahlungsArt = ZahlungsArt.ECZahlung;

                    informiereUeberAenderung();

                }
            });
        _ui.getEnterBut()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {

                    informiereUeberAenderung();
                }
            });

    }

    //registiert die buttons des Numberpad, druch erstellung eines arrays hinzufüegen eines actionlisteners
    private void registriereNumberPadAktionen()
    {

        for (int i = 0; i < getComponents(_ui.getZahlen()).length; i++)
        {
            JButton but = (JButton) getComponents(_ui.getZahlen())[i];
            but.addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {

                    setGezahltField(but.getText());

                }
            });
        }

        for (int i = 0; i < getComponents(_ui.getScheine()).length; i++)
        {
            JButton but = (JButton) getComponents(_ui.getScheine())[i];
            but.addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    if (_gezahltField.getText()
                        .equals(""))
                    {
                        _gezahltField
                            .setText(ergaenzeKommaStellen(but.getText()));

                        berechneRestbetrag(_gezahltField.getText());
                    }
                    else
                    {
                        _gezahltField.setText(addiereScheine(
                                ergaenzeKommaStellen(but.getText())));
                        berechneRestbetrag(_gezahltField.getText());
                    }

                }

            });

        }

    }

    //verarbeitet den input des gedrückten buttons, indem es den text des gezahlt feldes nimmt und behandelt einige sonderfälle durch switchcases
    private void setGezahltField(String text)
    {

        String ursprungText = _gezahltField.getText();

        String centanteil = "";

        if (ursprungText.contains("."))
        {
            centanteil = ursprungText.substring(ursprungText.indexOf("."));
        }

        boolean bocentanteil = centanteil.length() <= 2;

        {

            switch (text)
            {
            case "<-":
                if (ursprungText.length() < 1)
                {

                    break;

                }
                else
                {
                    _gezahltField.setText("");
                    ;
                    break;
                }

            case ".":
            {
                if (ursprungText.contains("."))
                {
                    break;
                }
                else
                {
                    _gezahltField.setText(ursprungText + text);
                    break;
                }
            }

            default:
                if (bocentanteil)
                {
                    _gezahltField.setText(ursprungText + text);
                    break;
                }
                else
                {
                    break;
                }

            }
        }

    }

    private void berechneRestbetrag(String eingabe)
    {
        Geldbetrag preis = new Geldbetrag(_preisFieldBezahlen.getText());

        Geldbetrag gezahlt = new Geldbetrag(eingabe);

        Geldbetrag differenz;

        if (gezahlt.BiggerEq(preis))
        {
            differenz = gezahlt.subtrahiere(preis);
            _ui.markiereRestBetrag(true);
        }
        else
        {
            differenz = preis.subtrahiere(gezahlt);
            _ui.markiereRestBetrag(false);
        }

        _restBetragField.setText(differenz.toString());
    }

    //addiert die Summe der gedrückten scheine zu einem neuen geldbetrag
    private String addiereScheine(String text)
    {
        Geldbetrag ersterSchein = new Geldbetrag(
                ergaenzeKommaStellen(_gezahltField.getText()));
        Geldbetrag neuerSchein = new Geldbetrag(text);
        return ersterSchein.addiere(neuerSchein)
            .getFormatiertenString();

    }

    //behandelt spezialfäält mit kommata und weggelassenen nullstellen durch ergänzung dieser
    private String ergaenzeKommaStellen(String text)
    {

        if (text.charAt(0) == '.' && text.length() == 2)
        {
            return '0' + text + '0';
        }
        else if (text.charAt(0) == '.' && text.length() == 3)
        {
            return '0' + text;
        }
        else if (!text.contains("."))
        {
            return text + ".00";
        }
        else if (text.substring(text.indexOf('.'))
            .length() == 2)
        {
            return text + '0';
        }
        else if (text.equals("."))
        {
            return '0' + text + "00";
        }
        else if (text.endsWith("."))
        {
            return text + "00";
        }
        return text;

    }

    private void registiereAktionenFuerMengenWerkzeug()
    {

        Component[] mengenPadArray = getComponents(_ui.getMengenPad());
        for (int i = 0; i < mengenPadArray.length; i++)
        {
            MengenButton but = (MengenButton) mengenPadArray[i];
            but.addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    but.getModel()
                        .setSelected(true);
                    _gedrueckteButtons.add(but);
                    if (_gedrueckteButtons.size() > 0)
                    {
                        _mehrereButtonsGedrueckt = true;

                    }

                }
            });

        }

    }

    public HashMap<String, Geldbetrag> getZahlungen()
    {
        HashMap<String, Geldbetrag> geldBetraege = new HashMap<String, Geldbetrag>();
        String rechnungsBetrag = "rechnungsBetrag";
        String zahlung = "zahlung";
        String rueckGeld = "rueckGeld";

        geldBetraege.put(rechnungsBetrag,
                new Geldbetrag(_preisField.getText()));
        geldBetraege.put(zahlung, new Geldbetrag(_gezahltField.getText()));
        geldBetraege.put(rueckGeld, new Geldbetrag(_restBetragField.getText()));

        return geldBetraege;

    }

    public void setzePreisField(Geldbetrag preis) throws NumberFormatException
    {

        _preisField.setText(preis.toString());
        _preisFieldBezahlen.setText(preis.toString());
        berechneRestbetrag("0.00");
        resetButtons();
        /**
                       catch (Exception e)
                       {
                           JDialog dia = new JDialog((Frame) null, e.getMessage());
                           dia.add(new JTextField(
                                   "Der Eingebene betrag überschreitet Integer Max Value"));
                           dia.pack();
                           dia.setLocationRelativeTo(null);
                           dia.setVisible(true);
                       
                       }*/

    }

    public int gedrueckteMenge()
    {

        if (!_mehrereButtonsGedrueckt)
        {

            return 1;

        }
        else
        {
            String menge = "";
            for (MengenButton but : _gedrueckteButtons)
            {
                menge = menge + but.getText();
            }
            if (menge.length() > 5)
            {
                return Integer.parseInt(menge.substring(0, 5));
            }

            return Integer.parseInt(menge);
        }

    }

    public void resetButtons()
    {
        for (MengenButton but : _gedrueckteButtons)

        {
            but.getModel()
                .setSelected(false);
        }
        _mehrereButtonsGedrueckt = false;
        _gedrueckteButtons.clear();
    }

    public void resetUI()
    {
        _ui.getEnterBut()
            .setEnabled(false);
        switchLayout(1);
        _gezahltField.setText(null);
        _preisField.setText("0.00");
    }

    private Component[] getComponents(JComponent comp)
    {
        Component[] comps = comp.getComponents();
        return comps;
    }

    public ZahlungsArt getZahlungsArt()

    {
        return _zahlungsArt;
    }

    private void switchLayout(int position)
    {
        String pos = "" + position;
        CardLayout cl = (CardLayout) _ui.getPanel()
            .getLayout();

        cl.show(_uiPanel, pos);
    }

    public JPanel getPanel()
    {
        return _ui.getPanel();
    }

}
