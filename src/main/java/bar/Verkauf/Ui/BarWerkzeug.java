package bar.Verkauf.Ui;

import bar.Services.BarLink;
import bar.Verkauf.Ui.SubwerkzeugObserver.ObservableKassenSystem;
import bar.Verkauf.Ui.SubwerkzeugObserver.SubwerkzeugObserver;

public class BarWerkzeug extends ObservableKassenSystem
{
    private Bar _bar;

    private BarWerkzeugUI _ui;

    private ProduktWerkzeug _produktPanel;

    private AuflisterWerkzeug _auflister;

    private NummernWerkzeug _numberPad;

    public BarWerkzeug(Bar bar)
    {
        assert bar != null : "Vorbedingung verletzt: null";

        _bar = bar;

        // Subwerkzeuge erstellen

        _produktPanel = new ProduktWerkzeug(_bar.getGetraenke(),
                _bar.getKategorien());

        _auflister = new AuflisterWerkzeug();
        _numberPad = new NummernWerkzeug();

        _ui = new BarWerkzeugUI(_produktPanel.getUI(), _auflister.getUI(),
                _numberPad.getPanel());

        erstelleSubwerkzeugListener();
        _ui.zeigeFenster();

    }

    private void erstelleSubwerkzeugListener()
    {

        _produktPanel.registriereBeobachter(new SubwerkzeugObserver()
        {

            @Override
            public void reagiereAufAenderung()
            {

                _auflister.fuegeProduktHinzu(
                        _produktPanel.getAusgewaehltesProdukt(),
                        _numberPad.gedrueckteMenge());

                _numberPad

                    .setzePreisField(_auflister.errechneGesamtbetrag());

            }
        });

        _numberPad.registriereBeobachter(new SubwerkzeugObserver()
        {

            @Override
            public void reagiereAufAenderung()
            {
                _auflister.dokumentiereZahlungen(_numberPad.getZahlungen(),
                        _numberPad.getZahlungsArt());
                _numberPad.resetUI();
                BarLink.handleBestellung(_auflister.getProdukte());
            }
        });

        _auflister.registriereBeobachter(new SubwerkzeugObserver()
        {

            @Override
            public void reagiereAufAenderung()
            {
                _numberPad

                    .setzePreisField(_auflister.errechneGesamtbetrag());
            }
        });
    }

}
