package bar.Verkauf.Ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class AuflisterWerkzeugUI
{
    private JList<String> _auflisterListe;
    private DefaultListModel<String> _bestellungen;
    private JPanel _panel;

    private JButton _stornoBut;
    private JButton _infoBut;
    private JButton _gesamtBut;

    private Dimension _fensterGr;

    public AuflisterWerkzeugUI(DefaultListModel<String> bestellungen)
    {
        setDimension();
        _bestellungen = bestellungen;

        _panel = erstellePanel();

    }

    public JPanel erstellePanel()
    {
        _panel = new JPanel();
        _panel.setLayout(new BorderLayout());

        //Umrandung        
        Border border = BorderFactory.createTitledBorder("Bestellung");
        _panel.setBorder(border);

        //JList hinzufuegen

        _panel.add(erstelleJList(), BorderLayout.CENTER);

        //Buttonsegment hinzufuegen

        _panel.add(erstelleButtons(), BorderLayout.SOUTH);

        return _panel;
    }

    private JList<String> erstelleJList()
    {

        _auflisterListe = new JList<String>(_bestellungen);

        return _auflisterListe;
    }

    public JPanel erstelleButtons()
    {
        Dimension butGr = new Dimension(0, (int) _fensterGr.getHeight() / 10);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 2));

        _stornoBut = new JButton("Storno");
        _gesamtBut = new JButton("Abrechnung");
        _infoBut = new JButton("Letzte Bestellung anzeigen");

        _stornoBut.setPreferredSize(butGr);
        _infoBut.setPreferredSize(butGr);

        buttonPanel.add(_infoBut);
        buttonPanel.add(_gesamtBut);
        buttonPanel.add(_stornoBut);

        buttonPanel.setPreferredSize(butGr);

        return buttonPanel;

    }

    public void zeigeInformationen(DefaultListModel<String> ansicht)

    {

        _auflisterListe.setModel(ansicht);

    }

    private Dimension setDimension()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        _fensterGr = toolkit.getScreenSize();

        return _fensterGr;
    }

    public JPanel getPanel()
    {
        return _panel;
    }

    public JButton getStornoBut()
    {
        return _stornoBut;
    }

    public JButton getInfoBut()
    {
        return _infoBut;
    }

    public JButton getGesamtBut()
    {
        return _gesamtBut;
    }

    public JList<String> getJlist()
    {
        return _auflisterListe;
    }

}
