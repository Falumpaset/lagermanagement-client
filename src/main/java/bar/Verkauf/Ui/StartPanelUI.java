package bar.Verkauf.Ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

public class StartPanelUI
{
    private JFrame _frame;

    private final static String Titel = "Nur der HSV";

    private Dimension _frameDim;

    private JButton _barButton;
  private JButton _barWerkzeugButton;
    private JButton _lagerButton;
    private JButton _managementButton;


    public StartPanelUI()
    {

        setUpFrame();

    }

    private void setUpFrame()
    {
        _frame = new JFrame(Titel);

        _frame.setLayout(new BorderLayout());
        _frame.setLocationRelativeTo(null);
        _frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setDimension();
        _frame.setSize(new Dimension((int) _frameDim.getWidth() / 3,
                (int) _frameDim.getHeight() / 2));
    
        _frame.add(setButtonPanel());
       _frame.setVisible(true);

        
     }
    
  

    private void setDimension()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        _frameDim = toolkit.getScreenSize();

    }

    public void verbergeUI()
    {
        _frame.dispose();
    }

    private JPanel setButtonPanel()
    {
         JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 1));
        _barButton = new JButton("1. Bar anmelden");
        _barWerkzeugButton = new JButton("2. Verkaufsansicht");
        _lagerButton = new JButton("3. Lager anmelden");
        _managementButton = new JButton("4. Lager Management");
        
        panel.add(_barButton);
        panel.add(_barWerkzeugButton);
        panel.add(_lagerButton);
        panel.add(_managementButton);
        Border border = BorderFactory.createTitledBorder("SetUp");
        panel.setBorder(border);
        
        return panel;
        
    }

    /**
    private List<JButton> erstelleButtons()
    {
        List<JButton> butList = new ArrayList<JButton>();
        
        butList.add(_barButton);
        butList.add(_barWerkzeugButton);
        butList.add(_lagerButton);
        butList.add(_managementButton);
        return butList;

    }
    */

    public JFrame getFrame()
    {
        return _frame;
    }

    public JButton getBarWerkzeugButton()
    {
    	
        return _barWerkzeugButton;
    }

    public JButton getLagerButton()
    {
        return _lagerButton;
    }

    public JButton getManagementButton()
    {
        return _managementButton;
    }

    public JButton getBarBut()
    {
        return _barButton;
    }
}
