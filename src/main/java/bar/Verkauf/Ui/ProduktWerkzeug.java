package bar.Verkauf.Ui;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeSet;

import javax.swing.DefaultListModel;
import javax.swing.JPanel;

import org.bson.types.ObjectId;

import bar.Fachwerte.Produkt;
import bar.Fachwerte.Produkt.Kategorie;
import bar.Services.BarLink;
import bar.Verkauf.Ui.SubwerkzeugObserver.ObservableSubwerkzeug;

public class ProduktWerkzeug extends ObservableSubwerkzeug {
	private ProduktWerkzeugUI _ui;

	private DefaultListModel<String> _infList;
	private Produkt _ausgewaehltesGetraenk;
	private boolean _informationenAngefordert;

	public ProduktWerkzeug(TreeSet<Produkt> produkte, TreeSet<Kategorie> kategorien) {
		_infList = new DefaultListModel<String>();
		_ui = new ProduktWerkzeugUI(produkte, kategorien, _infList);
		erstelleButtonListener();
		_informationenAngefordert = false;

	}

	public void erstelleButtonListener() {
		for (ProduktButton but : _ui.getProduktButtons()) {
			but.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					_ausgewaehltesGetraenk = but.getProdukt();
					if (_informationenAngefordert) {
						zeigeInformationen();
					} else {
						informiereUeberAenderung();
					}
				}
			});
		}
		for (KategorieButton but : _ui.getKategorieButtons()) {
			but.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					but.setSelected(true);

					switchLayout(but.getKategorie());
					for (KategorieButton b : _ui.getKategorieButtons()) {
						if (!b.equals(but)) {
							b.setSelected(false);
						}
					}
				}
			});
		}

		_ui.getInfoBut().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				_informationenAngefordert = true;
			}
		});
	}

	private void zeigeInformationen() {
		_infList.clear();
		for (ObjectId s : _ausgewaehltesGetraenk.getZutaten().keySet()) {
			_infList.addElement(BarLink.convertIdtoRohstoff(s).getName());
		}
		CardLayout cl = (CardLayout) _ui.getProduktPanel().getLayout();
		cl.show(_ui.getProduktPanel(), "information");
		_informationenAngefordert = false;
	}

	private void switchLayout(Kategorie kategorie) {

		CardLayout cl = (CardLayout) _ui.getProduktPanel().getLayout();

		cl.show(_ui.getProduktPanel(), kategorie.toString());
	}

	public Produkt getAusgewaehltesProdukt() {
		return _ausgewaehltesGetraenk;
	}

	public JPanel getUI() {
		return _ui.getPanel();
	}

}
