package bar.Verkauf.Ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import bar.Fachwerte.Geldbetrag;
import bar.Fachwerte.Produkt;
import bar.Verkauf.Ui.NummernWerkzeug.ZahlungsArt;
import bar.Verkauf.Ui.SubwerkzeugObserver.ObservableSubwerkzeug;


public class AuflisterWerkzeug extends ObservableSubwerkzeug
{
    private AuflisterWerkzeugUI _ui;

    private DefaultListModel<String> _stringLM;

    private HashMap<Produkt, Integer> _getSpeicher;

    private ArrayList<Produkt> _getArray;

    private DefaultListModel<String> _alterBon;

    private DefaultListModel<String> _alleBons;

    private Geldbetrag _gesamtBetrag;

    private boolean _altenBonAnzeigen;

    private boolean _informationenAngezeigt;

    private int elemt;

    public AuflisterWerkzeug()
    {
        erstelleSpeicher();
        _ui = new AuflisterWerkzeugUI(_stringLM);
        erstelleListener();

    }

    public void fuegeProduktHinzu(Produkt produkt, int anzahl)
    {
        if (_altenBonAnzeigen)
        {
            resetUI();
        }
        if (!_getSpeicher.containsKey(produkt))
        {
            _getSpeicher.put(produkt, anzahl);
            _stringLM.addElement(produkt.getFormatierenStringMenge(anzahl));
            _getArray.add(produkt);
        }
        else
        {
            anzahl += _getSpeicher.get(produkt);
            _getSpeicher.replace(produkt, anzahl);

            tauscheStringinLM(produkt, anzahl);
        }

    }

    private void stornoHandle(int index)
    {
        if (_stringLM.size() > 0)
        {
            String obj = _stringLM.get(index);
            obj = obj.substring(obj.indexOf(' ') + 1, obj.indexOf('€') + 1);

            for (Produkt get : _getSpeicher.keySet())
            {

                if (get.getFormatiertenString()
                    .equals(obj))
                {
                    _getSpeicher.remove(get);

                    _stringLM.remove(index);
                    _getArray.remove(index);

                    return;
                }
            }
        }

    }

    private void tauscheStringinLM(Produkt get, int anzahl)
    {

        int index = _getArray.indexOf(get);

        _stringLM.setElementAt(get.getFormatierenStringMenge(anzahl), index);

    }

    private void resetUI()
    {

        _getSpeicher.clear();
        _getArray.clear();
        _stringLM.clear();
        _getArray.clear();
        _altenBonAnzeigen = false;
    }

    private void erstelleListener()
    {

        _ui.getJlist()
            .addListSelectionListener(new ListSelectionListener()
            {

                @Override
                public void valueChanged(ListSelectionEvent e)
                {
                    elemt = e.getFirstIndex();

                }
            });
        _ui.getStornoBut()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {

                    stornoHandle(elemt);
                    informiereUeberAenderung();
                }
            });

        _ui.getInfoBut()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    if (_informationenAngezeigt)
                    {
                        _ui.zeigeInformationen(_stringLM);
                        _informationenAngezeigt = false;
                    }
                    else
                    {
                        _ui.zeigeInformationen(_alterBon);
                        _informationenAngezeigt = true;
                    }
                }
            });

        _ui.getGesamtBut()
            .addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    if (_informationenAngezeigt)
                    {
                        _ui.zeigeInformationen(_stringLM);
                        _informationenAngezeigt = false;
                    }
                    else
                    {
                        _alleBons.addElement(
                                "Erwarteter Kassenbestand:  " + _gesamtBetrag);
                        _ui.zeigeInformationen(_alleBons);
                        _informationenAngezeigt = true;
                    }
                }
            });

    }

    public void dokumentiereZahlungen(HashMap<String, Geldbetrag> geldbetraege,
            ZahlungsArt zahlungsart)
    {
        String rechnungsBetrag = "rechnungsBetrag";
        String zahlung = "zahlung";
        String rueckGeld = "rueckGeld";
        dokumentiereAlleZahlungen(geldbetraege.get(rechnungsBetrag),
                geldbetraege.get(rueckGeld));

        _stringLM.addElement("--------------------------------");
        _stringLM.addElement(
                "Gesamtbetrag:" + "  " + geldbetraege.get(rechnungsBetrag));
        _stringLM.addElement(
                zahlungsart.toString() + ":   " + geldbetraege.get(zahlung));
        _stringLM.addElement("Rueckgeld:  " + geldbetraege.get(rueckGeld));
        _altenBonAnzeigen = true;
        _alterBon.clear();
        for (int i = 0; i < _stringLM.getSize(); i++)
        {
            _alterBon.addElement(_stringLM.get(i));

        }

    }

    private void dokumentiereAlleZahlungen(Geldbetrag einzahlung,
            Geldbetrag auszahlung)
    {
        _alleBons.addElement("Einzahlung:  " + einzahlung);
        _alleBons.addElement("Auszahlung:  " + auszahlung);
        _alleBons.addElement("---------------------------");
        _gesamtBetrag = _gesamtBetrag.addiere(einzahlung);
    }

    private DefaultListModel<String> erstelleSpeicher()
    {
        _getSpeicher = new HashMap<Produkt, Integer>();
        _stringLM = new DefaultListModel<String>();
        _getArray = new ArrayList<Produkt>();
        _alterBon = new DefaultListModel<String>();
        _alleBons = new DefaultListModel<String>();
        _gesamtBetrag = new Geldbetrag(0);

        return _stringLM;
    }

    public Geldbetrag errechneGesamtbetrag()
    {

        Geldbetrag gesamtBetrag = new Geldbetrag(0, 00);
        for (Produkt get : _getSpeicher.keySet())
        {

            gesamtBetrag = gesamtBetrag
                .addiere(get.getPreisFuerMenge(_getSpeicher.get(get)));

        }

        return gesamtBetrag;
    }

    public JButton getStornobut()
    {
        return _ui.getStornoBut();
    }

    public JButton getInfoBut()
    {
        return _ui.getInfoBut();
    }

    public JPanel getUI()
    {
        return _ui.getPanel();
    }

    public HashMap<Produkt, Integer> getProdukte()
    {
        return _getSpeicher;
    }

}
