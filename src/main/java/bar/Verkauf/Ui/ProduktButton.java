package bar.Verkauf.Ui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;

import bar.Fachwerte.Produkt;

public class ProduktButton extends JButton

{

    /**
     * Ein mit einem Produkt verknüpfter Button
     * 
     */
    private static final long serialVersionUID = -6380540881978303052L;
    private Produkt _produkt;

    public ProduktButton(Produkt produkt)
    {
        super(produkt.getFormatiertenString());
        _produkt = produkt;
        setMaximumSize(setButtonGroeße());
    }

    public Produkt getProdukt()
    {
        return _produkt;
    }

    private Dimension setButtonGroeße()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension _fensterGr = toolkit.getScreenSize();

        return new Dimension((int) _fensterGr.getWidth() / 10,
                (int) _fensterGr.getHeight() / 10);
    }

}
