package bar.Services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;

import bar.Fachwerte.Bestellung;
import bar.Fachwerte.Produkt;
import bar.Fachwerte.Rohstoff;

public final class BarLink {

	private static HashMap<ObjectId, Rohstoff> hashSpeicher = new HashMap<>();

	public BarLink() {

	}

	public static void handleBestellung(Map<Produkt, Integer> produkte) {
		lagerBenachrichtigen(produkteInBestellungKonvertieren(produkte));
	}

	public static Bestellung produkteInBestellungKonvertieren(Map<Produkt, Integer> produkte) {
		var bestellung = new Bestellung();
		for (Produkt p : produkte.keySet()) {
			var rezept = p.getZutaten();
			for (ObjectId r : rezept.keySet()) {

				var menge = hashSpeicher.get(r).getAnteiligeMenge(rezept.get(r));
				if (bestellung.containsKey(r)) {
					bestellung.replace(r, (int) menge + bestellung.get(r));
				} else {
					bestellung.put(r, (int) Math.ceil(menge));
				}
			}

		}
		return bestellung;
	}

	private static void lagerBenachrichtigen(Bestellung verkaufteProdukte) {
		JerseyClient.productsSold(verkaufteProdukte);
	}

	public static List<Produkt> getProdukte() {
		List<Produkt> list = JerseyClient.getAllProduct();
		Set<String> rohstoffe = new HashSet<>();
		for (Produkt p : list) {
			for (ObjectId r : p.getZutaten().keySet()) {
				rohstoffe.add(r.toString());
			}
		}
		setHashSpeicher(new ArrayList<>(rohstoffe));

		return list;
	}

	private static void setHashSpeicher(List<String> rohstoffe) {

		List<Rohstoff> objects = JerseyClient.getAllRohstoffeById(rohstoffe);
		for (Rohstoff r : objects) {
			hashSpeicher.put(r.getId(), r);
		}
	}

	public static Rohstoff convertIdtoRohstoff(ObjectId id) {
		return hashSpeicher.get(id);
	}

}
