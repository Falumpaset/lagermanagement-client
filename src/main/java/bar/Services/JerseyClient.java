package bar.Services;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import bar.Fachwerte.Bestellung;
import bar.Fachwerte.Produkt;
import bar.Fachwerte.Rohstoff;
import bar.Mapper.ObjectIdMapping;

public class JerseyClient {

	public static final Client client = clientBuild();
	// private static final String REST_URI;

	private static Client clientBuild() {
		Client client = ClientBuilder.newClient();

		return client;
	}

	public static List<Produkt> getAllProduct() {
		List<Produkt> list = client.target("http://localhost:8080/db/produkte").path("getAll")
				.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Produkt>>() {
				});
		return list;

	}

	public static void productsSold(Bestellung bestellung) {

		client.target("http://localhost:8080/db").path("unstockbar").request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(bestellung, MediaType.APPLICATION_JSON));
	}

	public static Rohstoff getRohStoffById(ObjectId id) {
		Rohstoff stoff = client.target("http://localhost:8080/db/rohstoffe/get").path("" + id)
				.request(MediaType.APPLICATION_JSON).get(Rohstoff.class);
		return stoff;
	}

	public static List<Rohstoff> getAllRohstoffeById(List<String> ids) {
		
	
		return client.target("http://localhost:8080/db/rohstoffe").path("getAllById").request(MediaType.APPLICATION_JSON)
				.post(Entity.json(ids), new GenericType<List<Rohstoff>>() {
				});

	}

}
