package bar.Fachwerte;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public final class Menge
{
	
	
    private double menge;

    /**
     * Erstellt eine neue Menge abhängig der maßeinheit
     * @param menge die menge in cl
     */
   
    public Menge(double menge, String einheit)
    {
        switch (einheit)
        {
        case "l":
        	this.menge = menge * 100;
            break;

        case "ml":
        	this.menge = menge / 10;
            break;

        case "cl":
        	this.menge = menge;
        default:
        	this.menge = 1;
        }

    }
    
    @JsonCreator
    public Menge(@JsonProperty("menge") double menge)
    {
    	this.menge = menge;

    }
    

    @Override
    public boolean equals(Object obj)

    {
        boolean result = false;
        if (obj instanceof Menge)
        {
            Menge other = (Menge) obj;
            result = (getMenge() == other.getMenge());
        }
        return result;
    }

    public double getMenge()
    {
        return this.menge;
    }
    
    public void setMenge(double menge)
    {
    	this.menge = menge;
    }

    // Addieren von zwei Mengen
    public Menge addiere(Menge g)
    {
        return new Menge(getMenge() + g.getMenge(), "cl");
    }

    // Subtrahieren zweier Mengen
    public Menge subtrahiere(Menge g)
    {
        return new Menge(getMenge() - g.getMenge(), "cl");
    }

    public Menge multipliziere(double faktor)
    {
        return new Menge(getMenge() * faktor, "cl");
    }
}
