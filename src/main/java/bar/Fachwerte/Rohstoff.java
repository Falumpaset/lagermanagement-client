package bar.Fachwerte;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Rohstoff extends AbstractEintrag {

	private String name;

	private double einheitsGroeße;

	/**
	 * @require name != null
	 */

	public Rohstoff() {

	}

	public Rohstoff(String name) {
		assert name != null;
		this.name = name;
		this.einheitsGroeße = 1;

	}

	@JsonCreator
	public Rohstoff(@JsonProperty("name") String name, @JsonProperty("einhehitsGroeße") double einheitsGroeße) {
		this.name = name;
		this.einheitsGroeße = einheitsGroeße;

	}

	public double getAnteiligeMenge(Menge menge) {

		return (einheitsGroeße / 100 * menge.getMenge() / 100);
	}

	public double getEinheitsGroeße() {
		return einheitsGroeße;
	}

	public void setEinheitsGroeße(double neueGroeße) {
		this.einheitsGroeße = neueGroeße;

	}

	public String getName() {
		return this.name;
	}

	public void setName(String firstName) {
		this.name = firstName;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Rohstoff) {
			Rohstoff other = (Rohstoff) obj;
			result = (name.equals(other.name));
		}
		return result;
	}

	@Override
	public String toString() {
		return "Rohstoff [id=" + id + ",name=" + name + ",einheitsgroeße=" + einheitsGroeße + "]";
	}

}
