package bar.Fachwerte;

import java.util.HashMap;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Produkt extends AbstractEintrag implements ProduktService {


	private String name;

	private HashMap<ObjectId, Menge> zutaten = new HashMap<>();;

	private Geldbetrag preis;
	
	private double menge;

	private Kategorie typ;

	public enum Kategorie {
		CocktailAlkoholFrei, Cocktail, Flasche, FlascheAlkoholfrei, Allgemein;
	}

	public Produkt() {

	}

	@JsonCreator
	public Produkt(@JsonProperty("name") String name, @JsonProperty("zutaten") HashMap<ObjectId, Menge> zutaten,
			@JsonProperty("preis") Geldbetrag preis, @JsonProperty("kategorie") Kategorie kategorie,
			@JsonProperty("menge") double menge) {
		this.name = name;

		this.zutaten = zutaten;

		this.preis = preis;

		this.typ = kategorie;

		this.menge = menge;

	}

	
	public void fuegeZutatHinzu(Rohstoff produkt, Menge menge) {
		assert !enthaeltZutat(produkt);
		zutaten.put(produkt.getId(), menge);
	}

	
	public void entferneZutat(Rohstoff produkt) {
		assert enthaeltZutat(produkt);
		zutaten.remove(produkt.getId());
	}

	private boolean enthaeltZutat(Rohstoff produkt) {
		return zutaten.containsKey(produkt.getId());
	}

	
	public void setName(String neuerName) {
		name = neuerName;
	}

	
	public String getName() {
		return name;
	}

	
	public Kategorie getKategorie() {
		return typ;
	}

	
	public void setKategorie(Kategorie kategorie) {
		typ = kategorie;
	}


	
	public HashMap<ObjectId, Menge> getZutaten() {

		return zutaten;

	}
	
	public void setZutaten(HashMap<ObjectId, Menge> rezept)
	{
		this.zutaten = rezept;
	}

	@Override
	public double getMenge() {
		return menge;
	}

	@Override
	public Geldbetrag getPreis() {
		return preis;

	}

	public Geldbetrag getPreisFuerMenge(int menge) {

		return getPreis().multipliziere(menge);
	}

	@Override
	public void setPreis(Geldbetrag neuerPreis) {
		preis = neuerPreis;
	}

	@JsonIgnore
	@Override
	public String getFormatiertenString() {
		return getName() + " " + getMenge() + "l" + " " + getPreis() + "€";
	}
	
	@JsonIgnore
	public String getFormatierenStringMenge(int menge) {
		return menge + "x" + " " + getName() + " " + getMenge() + "l" + " " + getPreis() + "€" + " "
				+ getPreis().multipliziere(menge) + "€";
	}

	@Override
	public int hashCode() {
		return (int) (name.hashCode() + menge + preis.hashCode() + zutaten.hashCode() + typ.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Produkt) {
			Produkt eins = (Produkt) obj;
			return getPreis().equals(eins.getPreis()) && getName().equals(eins.getName())
					&& getKategorie().equals(eins.getKategorie()) && getZutaten().equals(eins.getZutaten());

		}
		return result;
	}

	@Override
	public String toString() {
		return "Produkt [id=" + id + ",name=" + name + ",menge" + menge + "]";
	}

}


