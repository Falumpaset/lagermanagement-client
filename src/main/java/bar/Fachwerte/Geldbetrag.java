package bar.Fachwerte;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;



/**
 * Ein Geldbetrag in Euro, gespeichert als ganze Euro- und ganze Cent-Beträge.
 * 
 * @author SE2-Team
 * @version SoSe 2015
 */

public final class Geldbetrag
{

    private  int euroAnteil;
    private  int centAnteil;
    
   
    public Geldbetrag()
    {
    	
    }

    /**
     * Wählt einen Geldbetrag aus.
     * 
     * @param eurocent Der Betrag in ganzen Euro-Cent
     * 
     * @require eurocent >= 0;
     */
    
    
    public Geldbetrag(int eurocent)
    {
        assert eurocent >= 0 : "Vorbedingung verletzt: eurocent >= 0";
        euroAnteil = eurocent / 100;
        centAnteil = eurocent % 100;
    }

    /**
     * Wählt einen Geldbetrag aus.
     * 
     * @param eurocent Der Betrag in Euro,Cent
     * 
     * @require euro >= 0 && cent >= 0;
     */
    
    @JsonCreator
    public Geldbetrag(@JsonProperty("euro")int euro, @JsonProperty("cent")int cent)
    {
        assert euro >= 0 && cent >= 0 : "Vorbedingung verletzt: eurocent >= 0";
        euroAnteil = euro;
        centAnteil = cent;
    }

    public Geldbetrag(String sbetrag)
    {
        sbetrag.replaceAll(",", ".");

        String euros = "";
        String cents = "";

        boolean atcents = false;

        //Wir gehen durch und fügen alles zu euros hinzu
        //Wenn wir ein . sehen dann atcents = true
        //dann wird alles darauffolgende den cents hinzugefügt

        //müll wird ignoriert
        for (int i = 0; i < sbetrag.length(); i++)
        {
            if (sbetrag.charAt(i) != '.'
                    && !Character.isDigit(sbetrag.charAt(i)))
            {
                continue;
            }

            if (sbetrag.charAt(i) == '.')
            {
                atcents = true;
                continue;
            }

            if (!atcents)
            {
                euros += sbetrag.charAt(i);
                continue;
            }

            cents = cents + sbetrag.charAt(i);
        }

        euroAnteil = Integer.parseInt(euros);
        centAnteil = Integer.parseInt(cents);

    }

    /**
     * Gibt den Eurobetrag ohne Cent zurück.
     * 
     * @return Den Eurobetrag ohne Cent.
     */
    public int getEuroAnteil()
    {
        return euroAnteil;
    }

    /**
     * Gibt den Centbetrag ohne Eurobetrag zurück.
     */
    public int getCentAnteil()
    {
        return centAnteil;
    }

    /**
     * Liefert einen formatierten String des Geldbetrags in der Form "10,23"
     * zurück.
     * 
     * @return eine String-Repräsentation.
     */
    @JsonIgnore
    public String getFormatiertenString()
    {
        return euroAnteil + "." + getFormatiertenCentAnteil();
    }

    /**
     * Liefert einen zweistelligen Centbetrag zurück.
     * 
     * @return eine String-Repräsentation des Cent-Anteils.
     */
    @JsonIgnore
    private String getFormatiertenCentAnteil()
    {
        String result = "";
        if (centAnteil < 10)
        {
            result += "0";
        }
        result += centAnteil;
        return result;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime + centAnteil;
        result = prime * result + euroAnteil;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if (obj instanceof Geldbetrag)
        {
            Geldbetrag other = (Geldbetrag) obj;
            result = (centAnteil == other.centAnteil)
                    && (euroAnteil == other.euroAnteil);
        }
        return result;
    }

    /**
     * Gibt diesen Geldbetrag in der Form "10,21" zurück.
     */
    @JsonIgnore
    @Override
    public String toString()
    {
        return getFormatiertenString();
    }

    
    @JsonIgnore
    public int getEuroCent()
    {
        return this.centAnteil + this.euroAnteil * 100;
    }

    // Addieren von zwei Geldbeträgen
    public Geldbetrag addiere(Geldbetrag g)
    {
        return new Geldbetrag(getEuroCent() + g.getEuroCent());
    }

    // Subtrahieren zweier Geldbeträge
    public Geldbetrag subtrahiere(Geldbetrag g)
    {
        return new Geldbetrag(getEuroCent() - g.getEuroCent());
    }

    //Vergleichen ob Betrag größer oder gleich  a
    public boolean BiggerEq(Geldbetrag a)
    {
        return getEuroCent() >= a.getEuroCent();
    }

    /**
     * 
     * @require faktor >= 0
     * @ensure Geldbetrag != null
     * 
     * @param faktor Wird mit dem Wert des Geldbetrag multipliziert
     * @return neuer Geldbetrag
     */
    public Geldbetrag multipliziere(double faktor)
    {
        assert faktor >= 0 : "Faktor ist negativ!";
        return new Geldbetrag((int) (getEuroCent() * faktor));
    }

}
