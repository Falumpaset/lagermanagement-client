package bar.Fachwerte;

import java.util.HashMap;

import org.bson.types.ObjectId;

import bar.Fachwerte.Produkt.Kategorie;



public abstract interface ProduktService
{
    public String getName();

    public Geldbetrag getPreis();

    public void setName(String neuerName);

    public void setPreis(Geldbetrag neuerPreis);

    public HashMap<ObjectId, Menge> getZutaten();

    public void setKategorie(Kategorie kategorie);

    public Kategorie getKategorie();

    public void fuegeZutatHinzu(Rohstoff rohstoff, Menge menge);

    public void entferneZutat(Rohstoff rohstoff);

    public double getMenge();

    public String getFormatiertenString();

}
