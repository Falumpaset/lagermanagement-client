package bar.Fachwerte;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import bar.Mapper.ObjectIdMapping;


public abstract class AbstractEintrag {

	
	
	protected ObjectId id;
	
	
	

	@SuppressWarnings("unused")
	private long version;
	
	public AbstractEintrag ()
	{
		super();
	}
	
	@JsonSerialize(using = ObjectIdMapping.class) 
	public ObjectId getId()
	{
		return id;
	}
	@JsonSerialize(using = ObjectIdMapping.class) 
	public void setId(ObjectId id)
	{
		this.id = id;
	}
	
	
	@Override
	public abstract String toString();
	
}
